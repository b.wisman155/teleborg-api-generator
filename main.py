from bs4 import BeautifulSoup
from urllib import request
import json
from objects import Field, Fields, Item, MyEncoder, METHOD, TYPE
from check import check
from generate import generate


def main():
    url = "https://core.telegram.org/bots/api"
    content = request.urlopen(url).read()
    soup = BeautifulSoup(content, "html.parser")

    item = soup.find(id="dev_page_content")
    types = item.find_all('h4')[4:]

    items = list()

    previous_h4 = None
    previous_p = None
    for i in item:
        if i.name == "h4": previous_h4 = i
        if i.name == "p" and previous_h4 is not None:
            previous_p = i
        if i.name == "table" and previous_p is not None:
            h = i.find("thead")
            t = TYPE if len(h.find_all("th")) == 3 else METHOD
            b = i.find("tbody")
            fields = Fields()
            for tr in b.find_all('tr'):
                field = list()
                required = True
                for td in tr.find_all('td'):
                    em = td.find('em')
                    if em is not None:
                        if em.text == "Optional":
                            required = False
                    field.append(td.text)
                fields.add(Field(field[0], # field name
                                 field[1], # field type
                                 field[2] == "Yes" if t == METHOD else required, # field required
                                 field[3] if t == METHOD else field[2])) # field description

            items.append(Item(previous_h4.text, previous_p.text, t, fields))

    #js = MyEncoder().encode(items)
    #file = open("api.json", 'w')
    #file.write(js)

    generate(items)
    #check(items)

if __name__ == "__main__":
    main()
