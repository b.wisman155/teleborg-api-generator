from objects import METHOD, TYPE, to_snake_case
import pathlib
import shutil
import textwrap

TAB = "    "
OK = 0
UNKNOWN = 1
FILE = 2
FILE_OR_STRING = 3
SKIP = 4

DOC_WIDTH = 80

exception_types = [(
    "InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply",
    "ReplyMarkup"), ("InlineKeyboardMarkup", "ReplyMarkup"),
                   ("InputMessageContent", "InputMessageContent"),
                   ("InputMedia", "InputMedia"), ("CallbackGame", "String")
                   ]  # !!!!!!todo PLACEHOLDER, TYPE NOT KNOWN
exception_fields = [("parse_mode", "ParseMode"), ("action", "ChatAction"),
                    ("reply_to_message", "Box<Message>"),
                    ("pinned_message", "Box<Message>"), ("limit", "u8"),
                    ("timeout", "u32"), ("results", "Vec<InlineQueryResult>"),
                    ("errors", "Vec<PassportElementError>")]
exception_imports = ["String", "i64", "bool", "u8", "u32"]
exclude_types = [
    "ReplyKeyboardMarkup", "ReplyMarkup", "InlineKeyboardMarkup",
    "InlineKeyboardButton", "KeyboardButton", "InlineQueryResult",
    "MessageContent"
]
include_types = ["KeyboardButtonPollType"]
exclude_type_exports = [
    "ReplyKeyboardMarkup", "InlineKeyboardMarkup",
    "PassportElementErrorDataField", "PassportElementErrorFile",
    "PassportElementErrorFiles", "PassportElementErrorFrontSide",
    "PassportElementErrorReverseSide", "PassportElementErrorSelfie",
    "PassportElementErrorTranslationFile",
    "PassportElementErrorTranslationFiles", "PassportElementErrorUnspecified"
]
include_type_exports = [
    "reply_markup::ReplyMarkup", "parse_mode::ParseMode",
    "chat_action::ChatAction", "passport_element_error::PassportElementError"
]
serialize_types = [
    "MaskPosition", "InlineQueryResult", "ShippingOption", "LabeledPrice", "ChatPermissions", "KeyboardButtonPollType"
]
exclude_methods = ["SendMediaGroup", "EditMessageMedia"]
rename_fields = [("type", "kind")]

items = []
types_to_import = []
method_return_types = []


# Function for making first char of string capital
def capital_first(s):
    return s[:1].upper() + s[1:] if s else ''


def remove_vec(s):
    return s.replace("Vec<", "").replace(">", "")


def init():
    global method_return_types
    shutil.rmtree('target', ignore_errors=True)
    pathlib.Path("target/types").mkdir(parents=True, exist_ok=True)
    pathlib.Path("target/methods").mkdir(parents=True, exist_ok=True)
    method_return_types = [
        line.rstrip('\n') for line in open('method_return_types')
    ]


def generate(t_items):
    global items
    items = t_items
    init()
    import_methods = []
    import_types = []

    for i in items:
        if i.f_type == METHOD:
            import_methods.append(
                "%s::%s" % (to_snake_case(i.name), capital_first(i.name)))
            generate_method(i)
        if i.f_type == TYPE:
            import_types.append("%s::%s" %
                                (to_snake_case(i.name), capital_first(i.name)))
            generate_type(i)

    methods = list(
        [x for x in import_methods if x.split("::")[1] not in exclude_methods])
    result = "pub use self::{\n%s\n};\n\n" % ", ".join(methods)
    for i in methods:
        result += "mod %s;\n" % i.split("::")[0]

    file = open("method_imports.txt", "w+")
    file.write(result)

    types = list([
        x for x in import_types
        if capital_first(x.split("::")[1]) not in exclude_types
    ]) + include_type_exports
    print(types)
    result = "pub use self::{\n%s\n};\n\n" % ", ".join(types)
    for i in types:
        result += "mod %s;\n" % i.split("::")[0]

    file = open("type_imports.txt", "w+")
    file.write(result)


def generate_method(item):
    global types_to_import
    # template = open("template_method", "r")
    # result = template.read()
    result = "use super::Method;\nuse serde::Serialize;\nuse typed_builder::TypedBuilder;\n\n"

    # Add Method docs, derives and the pub struct <Methodname> { line
    result += "/// " + item.description
    result += "\n#[derive(Debug, TypedBuilder, Serialize)]\n"

    result = generic_generate(result, item, False)

    multipart = [
        x for x in item.fields.fields if x.f_type.startswith("InputFile")
    ]
    if len(multipart) > 0:
        if len([x for x in item.fields.fields if x.field == "thumb"]):
            result += "impl_method_multipart_thumb!(%s, <replace>, \"%s\", \"%s\");\n" % (
                capital_first(item.name), item.name, multipart[0].field)
        else:
            result += "impl_method_multipart!(%s, <replace>, \"%s\", \"%s\");\n" % (
                capital_first(item.name), item.name, multipart[0].field)
    else:
        result += "impl_method!(%s, <replace>, \"%s\");\n" % (capital_first(
            item.name), item.name)

    for i in method_return_types:
        s = i.split(":")
        if s[0] == item.name:
            result = result.replace("<replace>", s[1])
            if s[1] not in ["bool", "i64"]:
                if "Vec<" in s[1]:
                    types_to_import.append(remove_vec(s[1]))
                else:
                    types_to_import.append(s[1])

    types_to_import = [
        remove_vec(i) for i in types_to_import if i not in exception_imports
    ]
    if len(types_to_import) > 0:
        result = result[:19] + "use crate::types::{" + \
            ", ".join(list(types_to_import)) + "};\n" + result[19:]
        result = "/// This code is generated using teleborg-api-generator (https://gitlab.com/b.wisman155/teleborg-api-generator)\n" + result
        types_to_import = []

    file = open('target/methods/%s.rs' % (to_snake_case(item.name)), "w")
    file.write(result)


def generate_type(item):
    global types_to_import

    for e in exclude_types:
        if (item.name.startswith(e) or item.name.endswith(e)) and item.name not in include_types:
            print("excluded: %s" % item.name)
            return
    if capital_first(item.name) in serialize_types:
        result = "use serde::{Deserialize, Serialize};\n\n"
        result += "/// %s\n" % item.description
        result += "#[derive(Clone, Deserialize, Serialize, Debug)]\n"
    else:
        result = "use serde::Deserialize;\n\n"
        result += "/// %s\n" % item.description
        result += "#[derive(Clone, Deserialize, Debug)]\n"

    result = generic_generate(result, item, True)

    distance = 36 if capital_first(item.name) in serialize_types else 23

    if len(types_to_import) > 0:
        result = result[:distance] + "use crate::types::{" + \
            ", ".join(list(set([i.replace("Box<", "").replace(">", "") for i in types_to_import if i not in exception_imports]))) + "};" + result[distance:]
        result = "/// This code is generated using teleborg-api-generator (https://gitlab.com/b.wisman155/teleborg-api-generator)\n" + result
        types_to_import = []

    file = open('target/types/%s.rs' % (to_snake_case(item.name)), "w")
    file.write(result)


def generic_generate(result, item, is_type):
    global types_to_import
    result += "pub struct %s {\n" % capital_first(item.name)

    # Loop through all fields and add
    for i in item.fields.fields:
        # if the field is not required, add derives for the builder to ignore
        # if empty

        # convert the field type to Rust type
        f_type = convert_type(i.f_type, i.required, i.field, item.name)

        # If type is unkown, try to find the type in the telegram types

        # Check if type is file
        if f_type[1] == FILE or f_type[1] == FILE_OR_STRING:
            result += (TAB + "#[builder(default)]\n") if not is_type else "" + TAB + \
                "#[serde(skip_serializing)]\n"
            result += TAB + "/// %s file to send with multipart\n" % i.field
            if i.field == "thumb":
                result += TAB + "pub thumb_file: Option<String>,\n"
            else:
                result += TAB + "pub file: Option<String>,\n"

            # Check if type is file but not file_or_string
            if f_type[1] == FILE:
                f_type[0] = SKIP
                f_type[1] = SKIP

        if (not i.required or f_type[1] == FILE_OR_STRING
            ) and f_type[1] is not SKIP and not is_type:
            result += TAB + "#[builder(default)]\n" + TAB + \
                "#[serde(skip_serializing_if = \"Option::is_none\")]\n"

        # Check if field must be skipped
        if f_type[1] != SKIP:
            if f_type[1] == UNKNOWN:
                print(item.name, i.field, "UNKNOWN")
            result += split_docs(i.description, True)
            if i.field in [x[0] for x in rename_fields]:
                result += TAB + "#[serde(rename = \"%s\")]\n" % i.field
                result += TAB + "pub %s: %s,\n" % (
                    [x[1]
                     for x in rename_fields if x[0] == i.field][0], f_type[0])
            else:
                result += TAB + "pub %s: %s,\n" % (i.field, f_type[0])

    result += "}\n\n"
    return result


def find_type(type_name):
    for i in items:
        if i.f_type == TYPE:
            if i.name == type_name:
                return type_name
    return UNKNOWN


def convert_type(t, required, field, name):
    global types_to_import
    formatter = "Option<%s>" if not required else "%s"

    if t == "errors":
        print(t, field, name)
    if t in [x[0] for x in exception_types]:
        temp = [x[1] for x in exception_types if t == x[0]][0]
        types_to_import.append(temp)
        return [formatter % temp, OK]

    if field in [x[0] for x in exception_fields]:
        temp = [x[1] for x in exception_fields if x[0] == field][0]
        if name != temp[temp.find("<") + 1:temp.find(">")]:
            types_to_import.append(temp)
        else:
            print(field + " - " + t)
        return [formatter % temp, OK]

    temp = find_type(t)
    # If type is found in telegram types. return
    if temp != UNKNOWN:
        types_to_import.append(temp)
        return [formatter % temp, OK]
    elif t == "Integer or String" or t == "Integer":
        return [formatter % "i64", OK]
    elif t == "String":
        return [formatter % "String", OK]
    elif t == "Float number" or t == "Float":
        return [formatter % "f64", OK]
    elif t == "Boolean" or t == "True":
        return [formatter % "bool", OK]
    elif t == "InputFile":
        return ["Option<%s>" % "String", FILE]
    elif t == "InputFile or String":
        return ["Option<%s>" % "String", FILE_OR_STRING]
    elif t.startswith("Array of Array of"):
        return [
            formatter % "Vec<Vec<%s>>" %
            convert_type(t.replace("Array of Array of ", ""), True, field, name)[0], OK
        ]
    elif t.startswith("Array of"):
        return [
            formatter % "Vec<%s>" %
            convert_type(t.replace("Array of ", ""), True, field, name)[0], OK
        ]
    else:
        return [UNKNOWN, UNKNOWN]


def split_docs(docs, tabbed):
    return (TAB if tabbed else "") + "/// %s\n" % docs
    result = ""
    for i in textwrap.wrap(docs, DOC_WIDTH, break_long_words=False):
        result += (TAB if tabbed else "") + "/// %s\n" % i
    return result
