from bs4 import BeautifulSoup
from termcolor import colored
from fuzzywuzzy import fuzz

from os import listdir
from os.path import isfile, join
import os.path
import re
import sys

from objects import METHOD, TYPE, to_snake_case

# Path of local library files
# types = "../teleborg/src/types"
# methods = "../teleborg/src/methods"
types = "target/types"
methods = "target/methods"

# Obtaining method and type file names, just the names
method_files = []
type_files = []

# Setting up exceptions that the checker must watch for becasue the types won't match
check_exceptions = [("parse_mode", "ParseMode"),
                    ("reply_markup", "ReplyMarkup"),
                    ("action", "ChatAction")]
# Setting up renamed fields that occur in the library files
check_renames = [("type_message_entity", "type")]
# Lines to skip when checking documentation
skip_lines = ["#[builder(default)]", "#[serde(skip_serializing_if = \"Option::is_none\")]", "#[derive(Debug, TypedBuilder, Serialize)]"]

# Check states for method/type fields
OK = "Ok"
INVALID = "Invalid"
NOT_FOUND = "Not Found"

# Check docs state
CHECK_DOCS = not "--nodocs" in sys.argv


# Main check method where the checking is started
# parameters: 
# `items`: The parsed methods/types from the telegram bot api site 
def check(items):
    method_files = [f for f in listdir(methods) if isfile(join(methods, f))]
    type_files = [f for f in listdir(types) if isfile(join(types, f))]

    # Checking Methods
    print(bold("Methods:"))
    check_methods(items, method_files)

    # Checking Types
    print(bold("\n\nTypes:"))
    check_types(items, type_files)


# Check methods function
# parameters: 
# `items`: The parsed methods/types from the telegram bot api site 
# `method_files`: The method file names obtained from local teleborg library
def check_methods(items, method_files):
    # Getting check results for methods
    result = check_files(items, method_files, METHOD)

    # Print out results for the user
    print("\nInvalid Methods")
    print(colored(list_to_string(result[1]), 'yellow'))

    print("\nMissing Methods")
    print(colored(list_to_string(result[0]), 'red'))

    print("\nPresent Methods")
    print(colored(list_to_string(result[2]), 'green'))
    print("------ %d/%d methods present --------" % (len(result[2]), len(result[2]) + len(result[0])))


# Check types function
# parameters: 
# `items`: The parsed methods/types from the telegram bot api site 
# `method_files`: The type files names obtained from local teleborg library
def check_types(items, type_files):
    # Getting check results for types
    result = check_files(items, type_files, TYPE)

    # Print out results for the user
    print("\nInvalid Types")
    print(colored(list_to_string(result[1]), 'yellow'))

    print("\nMissing Types")
    print(colored(list_to_string(result[0]), 'red'))

    print("\nPresent Types")
    print(colored(list_to_string(result[2]), 'green'))
    print("------ %d/%d types present --------" % (len(result[2]), len(result[2]) + len(result[0])))


# Start of checking the files
def check_files(items, files, type):
    # Setting up result variables
    missing_files = list()
    incomplete_files = list()
    present_files = list()

    # Check for possible typo's in file names
    for i in [m for m in items if m.f_type == type]:
        for f in files:
            ratio = fuzz.ratio(to_snake_case(i.name) + ".rs", f)
            if ratio > 95 and ratio < 100:
                print(colored("Warning %s looks like the required: %s. Is it a typo?" % (to_snake_case(i.name) + ".rs", f), "cyan"))

    # Validate the fields
    for i in [m for m in items if m.f_type == type]:
        # Convert the api Camelcase name to snake_case name. like: `GetUpdates` becomes `get_updates`
        s = to_snake_case(i.name)
        # Check if file exists
        if s + ".rs" in files:
            # Check if fields exists and if they are valid
            if check_fields(i.fields, "%s/%s.rs" % (methods if type == METHOD else types, to_snake_case(i.name)), i):
                present_files.append(i.name)
            else:
                incomplete_files.append(i.name)
        else:
            missing_files.append(i.name)

    return (missing_files, incomplete_files, present_files)


# Checking the fields itself
def check_fields(fields, file, item):
    file = open(file, 'r')
    text = file.read()

    text = text.replace("\n        ", "\n")
    text = text.replace("\n    ", "\n")

    for i, x in enumerate(skip_lines):
        text = text.replace(x + "\n", '')

    method = re.findall(r"\/\/\/\ (?P<docs>.+)\npub\ struct\ (?P<name>.+)\ {", text)
    if CHECK_DOCS:
        if len(method) == 0:
            print(colored("Warning file %s docs is MISSING. check it out bruv?" % (to_snake_case(item.name) + ".rs"), "cyan"))
        else:
            if item.description != method[0]:
                print(colored("Warning file %s docs don't match. check it out bruv?" % (to_snake_case(item.name) + ".rs"), "cyan"))

    result = re.findall(r"\/\/\/\ (?P<docs>.+)\npub\ (?P<name>.+):\ (?P<data_type>.+),", text)
    valid = True

    for i in fields.fields:
        status = NOT_FOUND
        for u in result:
            if status != OK:
                status = check_field(i, u, item.name)
        if status != OK:
            print(colored("Disaster Method: %s: Field %s: %s of type %s" % (bold(i.field), bold(status), bold(i.field), bold(i.f_type)), "magenta"))
            valid = False

    return valid


def check_field(field, file_field, method_name):
    name1 = field.field
    name2 = file_field[1]
    type1 = field.f_type
    type2 = file_field[2]
    is_option = False

    re_check = re.findall(r"Option<(.+)>", type2)
    if len(re_check) > 0:
        type2 = re_check[0]
        is_option = True

    if type1 == "InputFile":
        if name2 == "file":
            return OK

    renames = [x[1] for x in check_renames if x[0] == name2]
    if len(renames) > 0:
        name2 = renames[0]

    if name1 != name2:
        return NOT_FOUND

    if field.description != file_field[0] and CHECK_DOCS:
        print(colored("Warning file %s field `%s` docs don't match. check it out bruv?" % (to_snake_case(method_name) + ".rs", name1), "cyan"))

    if field.required and is_option or not is_option and not field.required:
        print("Optionallity wrong: ", method_name, name1)
        return INVALID

    if type1 == "Integer or String" or type1 == "Integer":
        if type2 in ["i64", "i32", "i16", "i8", "u64", "u32", "u16", "u8"]:
            return OK
    elif type1 == "String" and type2 == "String":
        return OK
    if type1 == "Boolean" and type2 == "bool":
        return OK
    if type1 == "InputFile or String":
        if type2 == "String":
            return OK
    if type1 == "Float number":
        if type2 in ["f64", "f32", "f16", "f8"]:
            return OK

    if to_snake_case(type1) in [x.replace('.rs', '') for x in type_files]:
        return OK

    if type1[0:8] == "Array of":
        if type2 == "Vec<%s>" % type1[9:]:
            return OK

    if name2 in [x[0] for x in check_exceptions if x[1] == type2]:
        return OK

    return INVALID


def list_to_string(lst):
    result = ""
    for i, x in enumerate(lst):
        s = x + ", "
        result += s if (i + 1) % 4 != 0 else s + '\n'

    return result


def bold(text):
    return '\033[1m' + text + '\033[0m'


def parse_file(path, f_type):
    file = open(os.path.abspath(path), 'r')
    text = file.read()
    soup = BeautifulSoup(text, "html.parser")
    print(soup.find())

