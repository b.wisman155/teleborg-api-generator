from json import JSONEncoder
import re

METHOD = "Method"
TYPE = "Type"

class Item:

    def __init__(self, name, description, f_type, fields):
        self.name = name
        self.description = description
        self.fields = fields
        self.f_type = f_type

    def __repr__(self):
        return "name:\t\t{}\ndescription:\t{}\ntype:\t\t{}\nfields:\t{}".format(self.name, self.description, self.f_type, str(self.fields))


class Fields:

    def __init__(self):
        self.fields = list()

    def add(self, field):
        self.fields.append(field)

    def __repr__(self):
        result = "\n"
        for i in self.fields:
            result += "\t{}\n".format(str(i))
        return result


class Field:

    def __init__(self, field, f_type, required, description):
        self.field = field
        self.f_type = f_type
        self.required = required
        self.description = description

    def __repr__(self):
        return "{}, {}, {}, {}".format(self.field, self.f_type, self.required, self.description[0:10] + "...")


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def to_snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
